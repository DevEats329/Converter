import os
import subprocess
import argparse
from module_importer import import_module



arg_parser = argparse.ArgumentParser(
    description="Convert files while keeping the directory structure intact",
    epilog="A list will be displayed with all the available presets"
)
arg_parser.add_argument(
    "-f", "--from-ext",
    required=True,
    help="select which files to convert (ex: webm)"
)
arg_parser.add_argument(
    "-t", "--to-ext",
    required=True,
    help="convert to this extension (ex: mp4)"
)



# Get arguments
args = arg_parser.parse_args()

# Parse arguments
args.from_ext = args.from_ext.removeprefix(".")
args.to_ext = args.to_ext.removeprefix(".")



# Get list of all available presets
all_presets = []
for i in os.listdir("./presets"):
    if os.path.isfile(f"./presets/{i}") == False:
        continue
    if i.startswith(f"{args.from_ext}_to_{args.to_ext}") == False:
        continue
    all_presets.append(
        i[ : i.rindex(".py")]
    )

if len(all_presets) == 0:
    print(f"Can't find preset for this conversion ({args.from_ext} -> {args.to_ext})")
    exit(0)



# Let the user choose what preset to use
print("Available presets:")
for i in range( len(all_presets) ):
    print(f"  {i} - {all_presets[i]}")
input_option = all_presets[ int( input("> ") ) ]



# Get the module that has the preset the user requested
presets_module = import_module(f"presets.{input_option}")



if os.path.isdir("./IN") == False:
    print("Creating directory 'IN'...")
    os.mkdir("./IN")
    print("Put the files in directory 'IN'")
    exit(0)



# Remove `OUT` dir only if it contains no files
if os.path.isdir("./OUT"):
    for _, _, files in os.walk("./OUT"):
        if len(files) != 0:
            print("Please remove directory 'OUT'")
            exit(1)
    subprocess.check_call([
        "rm",
        "-rf",
        "./OUT"
    ])

print("Cloning directory 'IN' to 'OUT'...")
subprocess.check_call([
    "cp",
    "-r",
    "./IN",
    "./OUT"
])



for path, _, files in os.walk("./OUT"):
    for f in files:
        if f.endswith(f".{args.from_ext}") == False:
            continue

        f_path_in = os.path.normpath(f"{path}/{f}")

        f_path_out = f_path_in.rstrip(f".{args.from_ext}")
        if f_path_out.endswith(f".{args.to_ext}") == False:
            f_path_out = f"{f_path_out}.{args.to_ext}"

        print("___________", len(f_path_in)*"_", "___________", sep="")
        print(f"----====== {f_path_in} ======----\n")

        try:
            presets_module.__convert(f_path_in, f_path_out)
        except subprocess.CalledProcessError as e:
            print(f"Converter exited with error code: {e.returncode}")
            print(f"Command arguments: {e.cmd}")
            continue
        except:
            exit(1)

        print(f"Removing: {f_path_in}")
        subprocess.check_call([
            "rm",
            f_path_in
        ])

        print()

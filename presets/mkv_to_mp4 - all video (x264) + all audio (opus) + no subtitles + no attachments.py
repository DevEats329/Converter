import subprocess



def __convert(f_path_in, f_path_out):
    subprocess.check_call([
        "ffmpeg",
        "-nostdin", "-hide_banner",
        "-i", f_path_in,
        "-map", "0",            # Map (select) all streams
        "-codec:v", "libx264",  # Convert video streams to AVC (x264)
        "-codec:a", "libopus",  # Convert audio streams to opus
        "-sn",                  # Don't copy subtitles
        "-dn",                  # Don't copy additional data (attachment) (default)
        #"-b:v", "",            # Video bitrate
        #"-b:a", "48k",         # Audio bitrate
        "-crf", "23",
        f_path_out
    ])

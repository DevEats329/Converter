import subprocess



def __convert(f_path_in, f_path_out):
    s_path_out = f"{f_path_out[:f_path_out.rindex('.')]}.srt"

    subprocess.check_call([
        "bash",
        "./utils/extract-all-subtitles.sh",
        f_path_in
    ])

    subprocess.check_call([
        "ffmpeg",
        "-nostdin", "-hide_banner",
        "-i", f_path_in,
        "-map", "0",            # Map (select) all streams
        "-codec:v", "vp9",      # Convert video streams to vp9
        "-codec:a", "libopus",  # Convert audio streams to opus
        "-sn",                  # Don't copy subtitles
        "-codec:d", "copy",     # Copy data (attachements) (default)
        "-crf", "23",
        f_path_out
    ])

import subprocess



def __convert(f_path_in, f_path_out):
    subprocess.check_call([
        "bash",
        "./utils/webp-to-gif.sh",
        f_path_in,
        f_path_out
    ])

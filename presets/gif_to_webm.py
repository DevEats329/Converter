import subprocess



def __convert(f_path_in, f_path_out):
    subprocess.check_call([
        "ffmpeg",
        "-nostdin", "-hide_banner",
        "-i", f_path_in,
        "-filter_complex", "[0:v] fps=24",
        "-f", "webm",
        "-vsync", "0",
        "-pix_fmt", "rgb24",
        f_path_out
    ])

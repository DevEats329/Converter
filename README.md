# Description

An attempt at combining multiple python and bash conversion scripts that I had laying around into one modular python CLI script



# Usage

```
$ python3 ./main.py --help
usage: main.py [-h] -f FROM_EXT -t TO_EXT

Convert files while keeping the directory structure intact

options:
  -h, --help                            show this help message and exit
  -f FROM_EXT, --from-ext FROM_EXT      select which files to convert (ex: webm)
  -t TO_EXT, --to-ext TO_EXT            convert to this extension (ex: mp4)

A list will be displayed with all the available presets
```



# Example

Put all your files inside a dir named "IN". The directory structure should look like this:
```
├── presets
│   └── ...
├── IN
│   └── >>>PUT THE FILES HERE<<<
├── LICENSE
├── main.py
├── module_importer.py
├── README.md
└── utils
    └── ...
```

```
$ python3 ./main.py -f mkv -t mp4
Available presets:
  0 - mkv_to_mp4 - all video (x264) + all audio (opus) + no subtitles + no attachments
  1 - mkv_to_mp4 - all video (x264) + all audio (opus) + extract all subtitles + copy all attachments
> 
```
After you select an option it will start to convert.

What the script does:
1. It will clone directory "IN" and rename it to "OUT"
1. Pick an "mkv" file in "OUT" and start converting
1. After it's done, remove the "mkv" file from "OUT"
1. Repeat from step 2

At the end you should have something like this:
```
├── IN
│   └── The WILD Thornberrys
│       ├── Baz.txt
│       ├── Season 1
│       │   ├── The WILD Thornberrys - S01 E01 - Flood Warning.mkv
│       │   ├── The WILD Thornberrys - S01 E02 - Dinner With Darwin.mkv
│       │   ├── The WILD Thornberrys - S01 E03 - Bad Company.mkv
│       │   ├── The WILD Thornberrys - S01 E04 - Gold Fever.mkv
│       │   ├── The WILD Thornberrys - S01 E05 - Matadi or Bust.mkv
│       │   └── ...
│       └── Some other files
│           ├── Bar.txt
│           └── Foo.txt
├── OUT
│   └── The WILD Thornberrys
│       ├── Baz.txt
│       ├── Season 1
│       │   ├── The WILD Thornberrys - S01 E01 - Flood Warning.mp4
│       │   ├── The WILD Thornberrys - S01 E02 - Dinner With Darwin.mp4
│       │   ├── The WILD Thornberrys - S01 E03 - Bad Company.mp4
│       │   ├── The WILD Thornberrys - S01 E04 - Gold Fever.mp4
│       │   ├── The WILD Thornberrys - S01 E05 - Matadi or Bust.mp4
│       │   └── ...
│       └── Some other files
│           ├── Bar.txt
│           └── Foo.txt
└── ...
```